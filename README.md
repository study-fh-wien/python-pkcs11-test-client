# OTP Client mit Smartcard Backend

###### tags: `3. Semester` `Mobile and Embedded Security` `OTP Client mit Smartcard Backend` `MVladislav`

---

<!-- TOC -->

- [OTP Client mit Smartcard Backend](#otp-client-mit-smartcard-backend) - [tags: `3. Semester` `Mobile and Embedded Security` `OTP Client mit Smartcard Backend` `MVladislav`](#tags-3-semester-mobile-and-embedded-security-otp-client-mit-smartcard-backend-mvladislav)
  - [Build](#build)
  - [Installation](#installation)
    - [dependencies](#dependencies)
  - [Konfiguration](#konfiguration)
    - [PKCS#11](#pkcs11)
  - [Betrieb](#betrieb)
    - [DEBUG](#debug)
  - [Ressources](#ressources)
    - [Hints](#hints)

<!-- /TOC -->

---

```md
    !!! RUN ONLY IF YOU NOW WHAT YOU DO !!!

    !!! EXPERIMENTAL DEVELOPMENT BUILD ... ;) !!!
```

---

## Build

nothing to build, it is a python project. Go on with Installation ;)

## Installation

### dependencies

install `pkcs#11`:

```sh
$pip3 install python-pkcs11
```

install `pyotp`, should be RFC 6238 conform, read <https://github.com/pyauth/pyotp>:

```sh
$pip3 install pyotp
```

qr code generator:

```sh
$pip3 install pyqrcode
```

scheduler:

```sh
$pip3 install schedule
```

## Konfiguration

### PKCS#11

setup `pkcs#11` as os env, replace `<path to .so>` with the path to the `pkcs#11 .so` file:

_(HINT: can be for example `/usr/lib/x86_64-linux-gnu/opensc-pkcs11.so`)_

```sh
$export PKCS11_MODULE=<path to .so>
```

## Betrieb

run with python explicit:

```sh
$python3 client.py
```

or direct:

```sh
: 'only needed when is not always executable'
$chmod u+x client.py

: 'run the program'
$./client.py
```

---

### DEBUG

```sh
$export PKCS11_MODULE=/usr/lib/x86_64-linux-gnu/pkcs11-spy.so
$export PKCS11SPY=/usr/lib/x86_64-linux-gnu/opensc-pkcs11.so
$export PKCS11SPY_OUTPUT=~/Desktop/log.log
$./client.py
```

---

## Ressources

- <https://tools.ietf.org/html/rfc4226>
- <https://tools.ietf.org/html/rfc6238>
- <https://docs.oasis-open.org/pkcs11/pkcs11-curr/v2.40/errata01/os/pkcs11-curr-v2.40-errata01-os-complete.html#_Toc441850684>
- <https://github.com/danni/python-pkcs11>
  - <https://pypi.org/project/python-pkcs11>
  - <https://python-pkcs11.readthedocs.io/en/latest/applied.html>
  - <https://python-pkcs11.readthedocs.io/en/latest/api.html>
- <https://github.com/pyauth/pyotp>
  - <https://pypi.org/project/pyotp/>
- <https://github.com/OpenSC/OpenSC/wiki/Using-OpenSC>

### Hints

| Short    | Long/Info                                                                             |
| :------- | :------------------------------------------------------------------------------------ |
| TOTP     | Time-based One-time Password                                                          |
| PKCS#11  | Public-Key Cryptography Standards                                                     |
| RFC 6238 | TOTP: Time-Based One-Time Password Algorithm <https://tools.ietf.org/html/rfc6238>    |
| RFC 4226 | HOTP: An HMAC-Based One-Time Password Algorithm <https://tools.ietf.org/html/rfc4226> |
