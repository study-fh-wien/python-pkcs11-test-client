#!/usr/bin/env python3
import base64
import json
import os
import struct
import sys
import time
from binascii import hexlify, unhexlify
from contextlib import suppress
from getpass import getpass
from json import JSONEncoder
from pprint import pprint

import pyqrcode
import schedule

try:
    from enum import Enum, IntEnum
except ImportError:
    from aenum import IntEnum, Enum

import pkcs11
import pyotp
from pkcs11 import (PROTECTED_AUTH, Attribute, AttributeTypeInvalid, KeyType,
                    Mechanism, MechanismFlag, MultipleObjectsReturned,
                    MultipleTokensReturned, NoSuchKey, NoSuchToken, Object,
                    ObjectClass, PKCS11Error, Session, Slot, UserType)
from pkcs11.defaults import (DEFAULT_GENERATE_MECHANISMS,
                             DEFAULT_KEY_CAPABILITIES)
from pkcs11.util.rsa import (decode_rsa_private_key, decode_rsa_public_key,
                             encode_rsa_public_key)

OS_ENV = "PKCS11_MODULE"


class OTP_FORMAT(IntEnum):
    OTP_FORMAT_DECIMAL = 0
    OTP_FORMAT_HEXADECIMAL = 1
    OTP_FORMAT_ALPHANUMERIC = 2
    OTP_FORMAT_BINARY = 3


class OTP_LENGTH(IntEnum):
    OTP_LENGTH_6 = 6
    OTP_LENGTH_7 = 7
    OTP_LENGTH_8 = 8


class OTP_TIME_INTERVAL(IntEnum):
    OTP_TIME_INTERVAL_30 = 30
    OTP_TIME_INTERVAL_60 = 60


class OTP_ATTRIBUTES(IntEnum):
    OTP_KEY = 0x00000001
    OTP_FORMAT = 0x00000002
    OTP_LENGTH = 0x00000003
    OTP_TIME_INTERVAL = 0x00000004
    OTP_USER_FRIENDLY_MODE = 0x00000005


OTP_OBJECT_DEFAULT = {
    OTP_ATTRIBUTES.OTP_KEY: None,
    OTP_ATTRIBUTES.OTP_FORMAT: OTP_FORMAT.OTP_FORMAT_DECIMAL,
    OTP_ATTRIBUTES.OTP_LENGTH: OTP_LENGTH.OTP_LENGTH_6,
    OTP_ATTRIBUTES.OTP_TIME_INTERVAL: OTP_TIME_INTERVAL.OTP_TIME_INTERVAL_30,
    OTP_ATTRIBUTES.OTP_USER_FRIENDLY_MODE: True,
}


def main():
    """
    ...
    """

    print("")
    print("################################")
    print("")

    lib = load_pkcs11_and_access_device()

    print("")
    print("################################")
    print("")

    token_label = search_and_list_available_token_labels(lib)

    print("")
    print("################################")
    print("")

    print("[I] load token...")
    try:
        token = lib.get_token(token_label=token_label)
        print(f"[I] Loaded token: {token}")
    except NoSuchToken:
        print(f"[E] Failed to find token: '{token_label}'")
    except MultipleTokensReturned:
        print(f"[E] Returns multiple token for: '{token_label}'")

    try:
        while True:
            try:
                print()
                print("################################")
                print()

                print(f"[I] Available modes:")
                print(f"    - 1: Read some infos")
                print(f"    - 2: write new key")
                print(f"    - 3: generate OTP")
                print(f"    - 4: verify OTP")
                print(f"    - 0: EXIT")
                mode = input("[O] Select mode: ")
                print()

                if mode == "0":
                    sys.exit(0)

                if mode != "1" and mode != "2" and mode != "3" and mode != "4":
                    print(f"[I] Mode '{mode}' is not supported!!!")
                    continue

                user_pin_needed = ""
                while user_pin_needed != "N" and user_pin_needed != "Y" and user_pin_needed != "P":
                    user_pin_needed = input("[O] Will you enter a user pin? (Y/n/p): ").upper()
                    user_pin_needed = "Y" if user_pin_needed == "" else user_pin_needed

                # if user entered p set to PROTECTED_AUTH
                user_pin = PROTECTED_AUTH if user_pin_needed == "P" else user_pin_needed
                # if user entered y let him enter a password
                user_pin = getpass("[O] User Pin [hidden](empty will use default pw: 123456): ").upper() if user_pin_needed == "Y" else None
                # if user entered a empty password and he has say y then user a default password
                user_pin = "123456" if user_pin_needed == "Y" and user_pin == "" else user_pin
                # so_pin = None

                # a label for the new secret
                label = input("[O] Label (default: test): ") if mode == "2" else None
                label = "test" if label == "" else label

                # will saved (in base32) as secret for the OTP generation
                data = input("[O] Secret PW (default: random): ") if mode == "2" else None
                data = data if data != "" else None

                # mode to open a session read or also write
                rw = True if mode == "2" else False

                # interactive = True if (input("[O] Interactive: ").upper() == "Y" if mode == "2" else False) else False

                print()
                print("[I] open session...")
                print()
                # Open a session on our token

                with token.open(rw=rw, user_pin=user_pin) as session:
                    # INFOS
                    if mode == "1":
                        load_session_infos(session)

                    # WRITE
                    elif mode == "2":
                        write_secret(session, user_pin, data, label)

                    # OTP
                    elif mode == "3":
                        generate_otp(session)

                    # VERIFY
                    elif mode == "4":
                        generate_otp(session, True)

            except KeyboardInterrupt:
                continue
            except PKCS11Error as e:
                print(f"[E] There was an error by mode {mode}!!! {repr(e)} ### {e}")
            except Exception as e:
                print(f"[E] {repr(e)} ### {e}")
    except PKCS11Error as e:
        print(f"[E] There was an error by select and/or work with mode!!! {repr(e)} ### {e}")
    except KeyboardInterrupt:
        sys.exit(0)
    except Exception as e:
        print(f"[E] {repr(e)} ### {e}")

#############################################################################
#############################################################################
#############################################################################


def load_pkcs11_and_access_device():
    """ returns the pkcs11 lib as object

    This function will get the path from pkcs#11 by defined os env
    and try to access a connected device
    and will return the lib to work on
    """

    print("[I] init PKCS#11...")

    lib = None
    try:
        # Initialise our PKCS#11 library
        lib = pkcs11.lib(os.environ[OS_ENV])  # export PKCS11_MODULE=/usr/lib/x86_64-linux-gnu/opensc-pkcs11.so

    except KeyboardInterrupt:
        sys.exit(0)
    except Exception as e:
        print(f"[E] Failed to load or access os env: '{OS_ENV}'!!! {repr(e)} ### {e}")
        print(f"    - HINT: run eg. $export PKCS11_MODULE=<path to .so>!!!")
        print(f"      - example: $export PKCS11_MODULE=/usr/lib/x86_64-linux-gnu/opensc-pkcs11.so!!!")
        sys.exit(0)
    return lib


def search_and_list_available_token_labels(lib):
    """ return the token label

    this function will search for "all" available token labels
    and print them to choose one, where we can work with in
    further steps
    """

    print("[I] try find available token labels...")

    token_label = None
    try:
        print("[I] Available Token Labels:")
        token_labels = []
        for idx, slot_xy in enumerate(lib.get_slots()):
            slot: Slot = slot_xy
            token: Object = slot.get_token()

            token_labels.append(token.label)
            print(f"    - {idx + 1}: {token.label}")
            if False:
                # print(f"    - : {slot.manufacturer}")
                print(f"      - slot_id         : {slot.slot_id}")
                print(f"      - slot_description: {slot.slot_description}")
                print(f"      - manufacturer_id : {slot.manufacturer_id}")
                print(f"      - hardware_version: {slot.hardware_version}")
                print(f"      - firmware_version: {slot.firmware_version}")
                print(f"      - flags           : {slot.flags}")
                print(f"      - manufacturer_id : {token.manufacturer_id}")
                print(f"      - model           : {token.model}")
                print(f"      - serial          : {token.serial}")
                print(f"      - flags           : {token.flags}")
                print(f"      - mechanism       :")
                # manu = ", ".join(sorted({Mechanism(mechanism).name for mechanism in slot.get_mechanisms()}))
                ({print(f"        - {Mechanism(mechanism).name} \n            {slot.get_mechanism_info(mechanism).flags}") for mechanism in slot.get_mechanisms()})
                # print(f"      - : {manu}")
        print(f"    - 0: EXIT")

        if (len(token_labels) <= 0):
            print("[I] No token lables found!!!")
            sys.exit(0)

        token_label_num = -1
        while token_label_num < 0 or token_label_num > len(token_labels):
            try:
                token_label_num = int(input(f"[O] Choose Token Label [0 - {len(token_labels)}]: "))
            except ValueError:
                pass
            if token_label_num == 0:
                sys.exit(0)
        token_label = token_labels[token_label_num-1]
        print(f"[I] Selected Token Label: {token_label}")

    except KeyboardInterrupt:
        sys.exit(0)
    except Exception as e:
        print(f"[E] There was an error by searching for token labels!!! {repr(e)} ### {e}")
    return token_label

#############################################################################
#############################################################################
#############################################################################


def load_session_infos(session: Session):
    """
    This function will load some information from a current open session
    """

    print("[I] load session infos...")

    try:
        print("-------------------------------")
        print(f"[I] token    : {session.token}")
        print(f"[I] rw       : {session.rw}")
        print(f"[I] user-type: {UserType(session.user_type).name}")
        print(f"[I] objects:")
        print("-------------------------------")

        try:
            for indx, token_key in enumerate(session.get_objects()):
                print(f"    {indx}. token-key:")
                for value in Attribute:
                    nicePrint(token_key, value, "      - ")
                print("    +++++++++++++++++++++++++++++")
        except NoSuchKey:
            pass
        # except MultipleObjectsReturned:
        #     pass
        # except AttributeTypeInvalid:
        #     pass
        # except StopIteration:
        #     pass
        except BaseException as e:
            print(f"[E] {repr(e)} ### {e}")
        print("-------------------------------")

    except KeyboardInterrupt:
        sys.exit(0)
    except Exception as e:
        print(f"[E] There was an error by access session infos!!! {repr(e)} ### {e}")


def write_secret(session, user_pin, data, label):
    """
    write secret and information to a token,
    to use it later for generating otp's
    """

    otp_object = OTP_OBJECT_DEFAULT
    otp_object[OTP_ATTRIBUTES.OTP_KEY] = base64.b32encode(data.encode()).decode() if data != None else pyotp.random_base32()
    otp_o = pyotp.TOTP(otp_object[OTP_ATTRIBUTES.OTP_KEY])

    objectClass = ObjectClass.DATA
    # keyType = KeyType.GENERIC_SECRET
    # if interactive:
    #     objectClass = getObjectClass()
    #     keyType = getKeyType()

    # session.generate_key(KeyType.GENERIC_SECRET, key_length=20,
    #                      id=None, label=label,
    #                      store=True, capabilities=MechanismFlag.DIGEST,
    #                      mechanism=Mechanism.SHA_1, mechanism_param=None,
    #                      template={
    #                          Attribute.KEY_TYPE: KeyType.GENERIC_SECRET,
    #                          Attribute.KEY_GEN_MECHANISM: Mechanism.SHA_1,
    #                          #  Attribute.LOCAL: True,
    #                          Attribute.SENSITIVE: True,
    #                          Attribute.EXTRACTABLE: False,
    #                          Attribute.VALUE_BITS: 160,
    #                          Attribute.VALUE: b"swordfish",
    #                          Attribute.DERIVE: True
    #                      })

    session.create_object({
        Attribute.CLASS: objectClass,
        # Attribute.KEY_TYPE: keyType,
        Attribute.TOKEN: True,
        Attribute.PRIVATE: False if user_pin == None else True,

        Attribute.LABEL: label or b'',

        # Attribute.DERIVE: True,
        # Attribute.ENCRYPT: False,
        # Attribute.DECRYPT: False,
        # Attribute.SIGN: True,
        # Attribute.VERIFY: True,
        # Attribute.WRAP: False,
        # Attribute.UNWRAP: False,

        Attribute.VALUE: json.dumps(otp_object).encode('utf-8'),

        Attribute.MODIFIABLE: False,
        Attribute.SENSITIVE: True,
        # Attribute.EXTRACTABLE: False
    })

    print("[I] TOTP Key successfully saved to token")
    print()
    print(f"[I] TOTP KEY QR :")
    print(pyqrcode.create(otp_o.provisioning_uri(name=label)).terminal(quiet_zone=1,))
    print(f"[I] TOTP KEY RAW: {otp_o.provisioning_uri(name=label)}")
    print(f"[D] TOTP: '{otp_o.now()}'")
    for key in OTP_ATTRIBUTES:
        print(f"      - {OTP_ATTRIBUTES(key).name}: {otp_object[key]}")


def generate_otp(session, verifyMode=False):
    try:
        session_object = load_session_object_for_choose_object(session)
        if session_object:
            # for value in Attribute:
            #     nicePrint(session_object, value)
            otp_object_json: bytes = session_object[Attribute.VALUE]
            # print(otp_object_json)
            otp_object_json = json.loads(otp_object_json)

            otp_object = OTP_OBJECT_DEFAULT
            # for key, value in otp_object_json.items():
            #     print(f"      - {key}: {value}")
            #     otp_object[int(key)] = value

            otp_object[OTP_ATTRIBUTES.OTP_KEY] = otp_object_json[str(OTP_ATTRIBUTES.OTP_KEY.value)]
            otp_object[OTP_ATTRIBUTES.OTP_FORMAT] = otp_object_json[str(OTP_ATTRIBUTES.OTP_FORMAT.value)]
            otp_object[OTP_ATTRIBUTES.OTP_LENGTH] = otp_object_json[str(OTP_ATTRIBUTES.OTP_LENGTH.value)]
            otp_object[OTP_ATTRIBUTES.OTP_TIME_INTERVAL] = otp_object_json[str(OTP_ATTRIBUTES.OTP_TIME_INTERVAL.value)]
            otp_object[OTP_ATTRIBUTES.OTP_USER_FRIENDLY_MODE] = otp_object_json[str(OTP_ATTRIBUTES.OTP_USER_FRIENDLY_MODE.value)]

            # print()
            # print(f"[D] TOTP Config:")
            # for key in OTP_ATTRIBUTES:
            #     print(f"      - {OTP_ATTRIBUTES(key).name}: {otp_object[key]}")

            try:
                if (verifyMode):
                    pin = pyotp.TOTP(otp_object[OTP_ATTRIBUTES.OTP_KEY])
                    while True:
                        data = input("[O] OTP PIN (stop with ctrl+c): ")
                        print(pin.verify(data))
                else:
                    print()
                    print(f"[I] Your TOTP will printed when a new one is available (stop with ctrl+c):")
                    print()
                    printOtp(otp_object)
                    schedule.every().minute.at(":31").do(printOtp, otp_object)
                    schedule.every().minute.at(":01").do(printOtp, otp_object)
                    while True:
                        schedule.run_pending()
                        time.sleep(1)
            except KeyboardInterrupt:
                pass
    except json.JSONDecodeError:
        print("[E] the token you choose can't used to create OTP")
    except UnicodeDecodeError:
        print("[E] the token you choose can't used to create OTP")


def load_session_object_for_choose_object(session: Session):
    """
    ...
    """

    session_object = None
    try:
        session_objects = []
        print("[I] Available Objects:")
        indx = 0
        try:
            for token_key in session.get_objects():
                try:
                    if token_key[Attribute.VALUE]:
                        indx += 1
                        session_objects.append(token_key)
                        print(f"    - {(indx)}: {token_key[Attribute.LABEL]}")
                except:
                    pass
        except:
            pass
        if (len(session_objects) <= 0):
            print("[I] No Objects found!!!")
        else:
            session_object_num = -1
            while session_object_num < 0 or session_object_num > len(session_objects):
                try:
                    session_object_num = int(input(f"[O] Choose Object [1 - {len(session_objects)}]: "))
                except ValueError:
                    pass
            session_object = session_objects[session_object_num-1]

    except KeyboardInterrupt:
        sys.exit(0)
    except Exception as e:
        print(f"[E] There was an error by searching for Objects!!! {repr(e)} ### {e}")
    return session_object

#############################################################################
#############################################################################
#############################################################################


def nicePrint(token_key: Object, val: Attribute, space):
    """
    ...
    """

    try:
        attr_val = token_key[val]
        if val == Attribute.CLASS:
            attr_val = ObjectClass(attr_val).name
        elif val == Attribute.VALUE or val == Attribute.OBJECT_ID:
            attr_val = hexlify(attr_val)
        print(f"{space}{Attribute(val).name}: {attr_val}")
    except:
        pass


def printOtp(otp_object):
    print(f"   * {pyotp.TOTP(otp_object[OTP_ATTRIBUTES.OTP_KEY]).now()}")


def getObjectClass():
    try:
        print(f"- 0: None")
        temp_arr = []
        for indx, oClass in enumerate(ObjectClass):
            print(f"- {indx+1}: {ObjectClass(oClass).name}")
            temp_arr.append(oClass)
        choose = int(input("[O] Which:"))
        choose = choose if choose >= 0 and choose < len(temp_arr) else 0
        return temp_arr[choose - 1] if choose != 0 else None
    except Exception as e:
        print(f"[E] {repr(e)} ### {e}")
    return None


def getKeyType():
    try:
        print(f"- 0: None")
        temp_arr = []
        for indx, kType in enumerate(KeyType):
            print(f"- {indx+1}: {KeyType(kType).name}")
            temp_arr.append(kType)
        choose = int(input("[O] Which:"))
        choose = choose if choose >= 0 and choose < len(temp_arr) else 0
        return temp_arr[choose - 1] if choose != 0 else None
    except Exception as e:
        print(f"[E] {repr(e)} ### {e}")
    return None

#############################################################################
#############################################################################
#############################################################################


if __name__ == "__main__":
    main()
